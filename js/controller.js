import Task from "./model.js";

export let takeNewTask = () => {
  const name = document.querySelector("#newTask").value;

  return new Task(name);
};

export let RenderTDL = (todoList) => {
  let contentTodoList = "";
  todoList.forEach((item) => {
    contentTodoList += `
  <li>${item.name}<span class="buttons">
  <button onclick="completeTask('${item.name}')" class="complete"><i class="fa fa-check-circle"></i></button>
  <button onclick="deleteToDoTask('${item.name}')" class="remove"><i class="fa fa-times-circle"></i></button>
  </span>
  </li>
    `;
    document.querySelector(".card__content > p").innerText = "";
    document.querySelector("#newTask").value = "";
  });
  document.querySelector("#todo").innerHTML = contentTodoList;
};

export let RenderCL = (completeList) => {
  let contentCompleteList = "";
  completeList.forEach((item) => {
    contentCompleteList += `
  <li>${item.name}<span class="buttons">
  <button onclick="deleteCompletedTask('${item.name}')" class="remove"><i class="fa fa-times-circle"></i></button>
  </span>
  </li>
    `;
  });
  document.querySelector("#completed").innerHTML = contentCompleteList;
};
