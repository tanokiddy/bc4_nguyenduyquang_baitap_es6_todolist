import { RenderCL, RenderTDL, takeNewTask } from "./controller.js";
import Task from "./model.js";

let completeList = [];
let todoList = [];

const TDL_LOCALSTORAGE = "TDL_LOCALSTORAGE";
const CL_LOCALSTORAGE = "CL_LOCALSTORAGE";

//get data from localstorage
let todoListJson = localStorage.getItem(TDL_LOCALSTORAGE);
let completeListJson = localStorage.getItem(CL_LOCALSTORAGE);

if (completeListJson != null) {
  completeList = JSON.parse(completeListJson);
  for (let index = 0; index < completeList.length; index++) {
    let item = completeList[index];
    completeList[index] = new Task(item.name);
  }
  RenderCL(completeList);
}

if (todoListJson != null) {
  todoList = JSON.parse(todoListJson);
  for (let index = 0; index < todoList.length; index++) {
    let item = todoList[index];
    todoList[index] = new Task(item.name);
  }
  RenderTDL(todoList);
}
//=================START TO DO TASKS=======================
//Add newTasks
document.querySelector("#addItem").onclick = function () {
  if (document.querySelector("#newTask").value == "") {
    document.querySelector(".card__content > p").innerText =
      "Vui lòng nhập tên Task";
  } else {
    let contentTodoList = takeNewTask();
    todoList.push(contentTodoList);
    //make json
    let todoListJson = JSON.stringify(todoList);
    //save json
    localStorage.setItem(TDL_LOCALSTORAGE, todoListJson);
    RenderTDL(todoList);
  }
};

//Delete ToDoTasks
export let deleteToDoTask = (id) => {
  let index = todoList.findIndex((item) => {
    return item.name == id;
  });
  console.log("index: ", index);
  if (index != -1) {
    todoList.splice(index, 1);
    let todoListJson = JSON.stringify(todoList);
    localStorage.setItem(TDL_LOCALSTORAGE, todoListJson);
    RenderTDL(todoList);
  }
};
window.deleteToDoTask = deleteToDoTask;
//======================END TO DO TASK=====================

//====================START COMPLETED TASKS================
//Add CompletedTasks
export let completeTask = (id) => {
  let index = todoList.findIndex((item) => {
    return item.name == id;
  });
  if (index != -1) {
    //add completed task
    let contentCompleteList = todoList[index];
    completeList.push(contentCompleteList);
    //make json
    let completeListJson = JSON.stringify(completeList);
    //save json
    localStorage.setItem(CL_LOCALSTORAGE, completeListJson);
    RenderCL(completeList);
    //delete todotask
    todoList.splice(index, 1);
    let todoListJson = JSON.stringify(todoList);
    localStorage.setItem(TDL_LOCALSTORAGE, todoListJson);
    RenderTDL(todoList);
  }
};
window.completeTask = completeTask;

//Delete CompletedTasks
export let deleteCompletedTask = (id) => {
  let index = completeList.findIndex((item) => {
    return item.name == id;
  });
  if (index != -1) {
    completeList.splice(index, 1);
    let completeListJson = JSON.stringify(completeList);
    localStorage.setItem(CL_LOCALSTORAGE, completeListJson);
    RenderCL(completeList);
  }
};
window.deleteCompletedTask = deleteCompletedTask;
//====================END COMPLETED TASKS================

//===================SET SORT BUTTON====================
document.querySelector("#two").onclick = function () {
  todoList.sort((a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    // names must be equal
    return 0;
  });
  let todoListJson = JSON.stringify(todoList);
  localStorage.setItem(TDL_LOCALSTORAGE, todoListJson);
  RenderTDL(todoList);
};

document.querySelector("#three").onclick = function () {
  todoList.sort((a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return 1;
    }
    if (nameA > nameB) {
      return -1;
    }
    // names must be equal
    return 0;
  });
  let todoListJson = JSON.stringify(todoList);
  localStorage.setItem(TDL_LOCALSTORAGE, todoListJson);
  RenderTDL(todoList);
};
